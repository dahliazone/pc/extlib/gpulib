#include "../../gpulib_imgui.h"
#include "../../gpulib.h"

int main() {
  SDL_Window * sdl_window = NULL;
  GpuWindow("Dear ImGui", 1280, 720, 4, 0, 0, &sdl_window, NULL);
  GpuSetDebugCallback(GpuDebugCallback, NULL);

  imgui_init();

  struct ImGuiIO * io = igGetIO();
  struct ImGuiStyle * style = igGetStyle();

  char font[10000] = {0};
  SDL_snprintf(font, 10000, "%s%s", SDL_GetBasePath(), "NotoSans.ttf");
  ImFontAtlas_AddFontFromFileTTF(io->Fonts, font, 24, NULL, NULL);

  style->ScrollbarRounding = 0;
  style->WindowRounding = 0;
  style->FrameRounding = 0;

  for (;;) {
    GpuClear();
    imgui_new_frame(sdl_window, true);

    {
      // Flat UI by yorick.penninks: https://color.adobe.com/Flat-UI-color-theme-2469224/
      static struct ImVec3 color_for_text = {236 / 255.f, 240 / 255.f, 241 / 255.f};
      static struct ImVec3 color_for_head = { 41 / 255.f, 128 / 255.f, 185 / 255.f};
      static struct ImVec3 color_for_area = { 57 / 255.f,  79 / 255.f, 105 / 255.f};
      static struct ImVec3 color_for_body = { 44 / 255.f,  62 / 255.f,  80 / 255.f};
      static struct ImVec3 color_for_pops = { 33 / 255.f,  46 / 255.f,  60 / 255.f};

      // Mint Y Dark:
      //static struct ImVec3 color_for_text = {211 / 255.f, 211 / 255.f, 211 / 255.f};
      //static struct ImVec3 color_for_head = { 95 / 255.f, 142 / 255.f,  85 / 255.f};
      //static struct ImVec3 color_for_area = { 47 / 255.f,  47 / 255.f,  47 / 255.f};
      //static struct ImVec3 color_for_body = { 64 / 255.f,  64 / 255.f,  64 / 255.f};
      //static struct ImVec3 color_for_pops = { 30 / 255.f,  30 / 255.f,  30 / 255.f};

      // Arc Theme:
      //static struct ImVec3 color_for_text = {211 / 255.f, 218 / 255.f, 227 / 255.f};
      //static struct ImVec3 color_for_head = { 64 / 255.f, 132 / 255.f, 214 / 255.f};
      //static struct ImVec3 color_for_area = { 47 / 255.f,  52 / 255.f,  63 / 255.f};
      //static struct ImVec3 color_for_body = { 56 / 255.f,  60 / 255.f,  74 / 255.f};
      //static struct ImVec3 color_for_pops = { 28 / 255.f,  30 / 255.f,  37 / 255.f};

      igColorEdit3("Text", &color_for_text.x);
      igColorEdit3("Head", &color_for_head.x);
      igColorEdit3("Area", &color_for_area.x);
      igColorEdit3("Body", &color_for_body.x);
      igColorEdit3("Pops", &color_for_pops.x);

      imgui_easy_theming(color_for_text, color_for_head, color_for_area, color_for_body, color_for_pops);
    }

    static bool show_test_window = true;
    igShowTestWindow(&show_test_window);

    igRender();
    GpuSwap(sdl_window);
    
    for (SDL_Event e; SDL_PollEvent(&e);) {
      imgui_process_event(&e);
      if (e.type == SDL_QUIT)
        goto exit;
    }
  }

exit:
  imgui_deinit();
  return 0;
}